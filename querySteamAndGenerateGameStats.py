#! /usr/bin/env python

# This script takes a plain text list of games by title name and queries the steam web api for each game
# It then gets the score of the game from steam and prints it out to console in a nicely formatted table
# Additionally it gets the tags of the game
# So the output columns contain these columns
# Game Title, Steam Review Score, Number of Reviews, List of Game Tags


# it outputs a nice table, and then also saves all the data to a python dictionary

import requests
from prettytable import PrettyTable
from secrets import steam_web_api_key, steam_profile_id
import json
import time
import sys
import os
import re
import unicodedata

DEBUG = False
not_found_games = []
potential_matching_names = {}


def debug(str):
    if DEBUG:
        print(str)


def update_progress(progress, total, game_title):
    """
    Updates the progress bar in the console.

    :param progress: The current progress (number of items processed).
    :param total: The total number of items to process.
    """
    # Calculate the percentage of completion
    percentage = 100 * (progress / total)
    # Calculate the number of blocks to display in the loading bar
    bar_length = 50  # You can adjust the length of the loading bar
    filled_length = int(bar_length * progress // total)

    # Create the bar string
    bar = '█' * filled_length + '-' * (bar_length - filled_length)

    # Print the progress bar with the percentage
    # Using carriage return to overwrite the current line
    # end='' prevents a new line from being added

    # clear the line
    sys.stdout.write('\x1b[2K')
    sys.stdout.write(f'\rProgress: |{bar}| {percentage:.2f}%')
    sys.stdout.flush()
    # move the line down so we don 't overwrite the first line
    sys.stdout.write('\x1b[B')
    sys.stdout.write('\x1b[2K')
    sys.stdout.write(f'\r({progress}/{total:}) {game_title}')
    sys.stdout.flush()  # Force flushing the output buffer
    # move the cursor back up one line 
    sys.stdout.write('\x1b[A')


def get_steam_games():
    """Fetch the list of all games from the Steam API that are for sale on steam."""
    if os.path.exists('applist.json'):
        with open('applist.json', 'r') as fd:
            data = json.load(fd)
            games_dict = {clean_words(game['name'].lower()): game['appid'] for game in data['applist']['apps']}
            return games_dict
    else:
        url = "https://api.steampowered.com/ISteamApps/GetAppList/v0002/?format=json"
        response = requests.get(url)
        if response.status_code == 200:
            data = response.json()
            # write the json table so I can easily query it later
            with open("applist.json", 'w') as fd:
                json.dump(data, fd, indent=4)
            # Convert the list of games to a dictionary for faster lookups
            games_dict = {clean_words(game['name'].lower()): game['appid'] for game in data['applist']['apps']}
            return games_dict
        else:
            debug("Failed to fetch the game list from Steam.")
            return {}


def get_all_player_owned_steam_games():
    # Construct the URL
    url = f'http://api.steampowered.com/IPlayerService/GetOwnedGames/v1/?key={steam_web_api_key}&steamid={steam_profile_id}&format=json'

    # Make the request
    response = requests.get(url)

    # Check if the request was successful
    if response.status_code == 200:
        # Parse the JSON response
        return response.json()
    else:
        raise "no games found"
        return {}


def get_game_app_id(name, games_dict):
    """Check if a game exists in the Steam games list."""
    name = name.lower()
    if name in games_dict:
        return games_dict[name]

    # else split the string into tokens and find the game with the highest match of tokens
    name_found = find_closest_keys(name, games_dict)
    if name_found:
        return games_dict[name_found]
    else:
        debug(f"Game not found: {name}")
        return None


def remove_diacritics(input_str):
    """
    Removes diacritics from a string, converting characters like 'Û' to 'U'.

    :param input_str: The string from which to remove diacritics.
    :return: A string with diacritics removed.
    """
    # Normalize to form 'NFD' to decompose characters into base characters and diacritics
    normalized = unicodedata.normalize('NFD', input_str)
    # Filter out the diacritics and recompose to a single string without them
    without_diacritics = ''.join(c for c in normalized if unicodedata.category(c) != 'Mn')
    return without_diacritics


def clean_words(sentence):
    """Remove non-alphanumeric characters from the sentence and split into words."""
    cleaned_sentence = remove_diacritics(sentence)
    cleaned_sentence = re.sub(r'[^A-Za-z0-9 ]+', '', cleaned_sentence)
    return cleaned_sentence.strip()


def find_closest_keys(search_phrase, dictionary):
    # Clean and tokenize the search phrase
    search_word = clean_words(search_phrase)

    # Iterate over dictionary keys to calculate match scores
    for key in dictionary.keys():
        # Clean and tokenize key
        cleaned_key = clean_words(key)
        if search_word == cleaned_key:
            return key

    # if still not found split on the - or semi colon and try again
    search_word = search_phrase.split(':')[0].split('-')[0]
    search_word = clean_words(search_word)
    found_id = dictionary.get(search_word)
    if found_id:
        return search_word

    # if still not found, try to filter out editions
    for key in dictionary.keys():
        # Clean and tokenize key
        cleaned_key = clean_words(key)
        if search_word == cleaned_key:
            return key

def find_closest_matching_names(search_phrase):
    # Clean and tokenize the search phrase
    search_word = clean_words(search_phrase)
    print(search_phrase)
    matches = []
    with open('applist.json', 'r') as file:
        for line in file:
            if re.search(search_word, line):
                print(f'\t{line.strip()}')
                matches.append(line.strip())
    return matches


def query_steam_api_by_title(game_title, games_dict):
    game_id = get_game_app_id(game_title, games_dict)
    if not game_id:
        not_found_games.append(game_title)
        return {
            'title': game_title,
            'appid': -1,
            'score': '',
            'recommendations': '',
            'number_of_reviews': '',
            'tags': {
                'description': [],
                'genres': []
            }
        } 
    return query_steam_api(game_id, games_dict)


def query_steam_api(app_id, games_dict):
    """
    Query the Steam Web API for a given game title to fetch review score, number of reviews, and game tags.
    Returns a dictionary with the fetched data.
    """
    # You'll need to replace `YOUR_STEAM_API_KEY` with your actual Steam API key and adjust the URL if needed
    if not app_id:
        not_found_games.append(app_id)
        return 

        # Once you have the game's appid, use it to get more details (example endpoint, might not be real)
    api_key = steam_web_api_key
    details_url = f'https://store.steampowered.com/api/appdetails?appids={app_id}&key={api_key}'
    details_response = requests.get(details_url).json()

    # sleep for some stability 
    time.sleep(0.5)

    debug(f'GET {details_url}')
    debug(json.dumps(details_response, indent=4))

    try:
        game_title = details_response[str(app_id)]['data']['name']
    except Exception as e:
        game_title = app_id

    # get the games review
    review_details = f'https://store.steampowered.com/appreviews/{app_id}?json=1&language=all'
    review_response = requests.get(review_details)
    review_response.encoding = 'utf-8-sig'
    try:
        review_response = review_response.json()
    except:
        print(f"Error decoding JSON for app_id {app_id}: {e}")
        return

    debug(json.dumps(review_response, indent=4))

    
    try:
        review_score_desc = review_response['query_summary']['review_score_desc']
    except Exception as e:
        review_score_desc = 'na'
    
    try:   
        game_details = details_response[str(app_id)]['data']
    except Exception as e:
        # else populate with empty data
        game_details = {
            'recommendations': '',
            'tags': {
                'description': [],
                'genres': []
            }
        }

    return {
        'title': game_title,
        'appid': app_id,
        'score': review_score_desc,
        'number_of_reviews': game_details.get('recommendations', {}),
        'tags': [tag['description'] for tag in game_details.get('genres', [])] + [tag['description'] for tag in
                                                                                  game_details.get('categories', [])]
        # Assuming genres can serve as tags
    }


def query_steam_api_by_app_id(app_id):
    """
    Query the Steam Web API for a given game title to fetch review score, number of reviews, and game tags.
    Returns a dictionary with the fetched data.
    """

        # Once you have the game's appid, use it to get more details (example endpoint, might not be real)
    api_key = steam_web_api_key
    details_url = f'https://store.steampowered.com/api/appdetails?appids={app_id}&key={api_key}'
    details_response = requests.get(details_url).json()

    debug(json.dumps(details_response, indent=4))

    # get the games review
    review_details = f'https://store.steampowered.com/appreviews/{app_id}?json=1&language=all'
    review_response = requests.get(review_details).json()
    debug(json.dumps(review_response, indent=4))

    review_score_desc = review_response['query_summary']['review_score_desc']

    try:
        game_details = details_response[str(app_id)]['data']
    except Exception as e:
        # else populate with empty data
        game_details = {
            'recommendations': '',
            'tags': {
                'description': [],
                'genres': []
            }
        }

    try:
        title = details_response[str(app_id)]['data']['name']
    except Exception as e:
        # not found game
        title = ''

    return {
        'title': title,
        'appid': app_id,
        'score': review_score_desc,
        'number_of_reviews': game_details.get('recommendations', {}),
        'tags': [tag['description'] for tag in game_details.get('genres', [])] + [tag['description'] for tag in
                                                                                  game_details.get('categories', [])]
        # Assuming genres can serve as tags
    }

# def get_owned_steam_games():
#     api_key = steam_web_api_key
#     player_id = player_steam_id
#     url = f"http://api.steampowered.com/IPlayerService/GetOwnedGames/v01/?key={api_key}&steamid=76561197960434622&format=json"

#     # TOOD should make profile unprivate and use this
#     url = "https://steamcommunity.com/id/purplemillipede/games?tab=all&xml=1"
#     review_response = requests.get(url)
#     print(review_response.content)
#     games = review_response.json()['response']['games']
#     return games

#     # then exclude wishlist games
#     # url = f"https://store.steampowered.com/wishlist/profiles/{player_id}/wishlistdata/?p=0"
#     # wishlist = requests.get(url)
#     # return wishlist.json()

def main(output_file_name):
    # Read owned game titles from a text file
    with open('gamelist/gameslist.txt', 'r') as file:
        game_titles = file.read().splitlines()

    try:
        with open(output_file_name, 'r') as gs:
            game_stats = json.load(gs)
    except FileNotFoundError:
        game_stats = {}

    
    # get owned steam games by the player in their account
    owned_steam_games = get_all_player_owned_steam_games()

    # Get the list of Steam games once
    steam_games = get_steam_games()

    table = PrettyTable()
    table.field_names = ['Game Title', 'Steam Review Score', 'Number of Reviews', 'List of Game Tags']
    table.max_width['List of Game Tags'] = 40

    # parse through the plain text list of games and add them to the json output
    i = 0
    total_games = len(game_titles) + owned_steam_games['response']['game_count']
    for title in game_titles:
        update_progress(i, total_games, title)
        i += 1
        game_data = game_stats.get(title)
        # only query steam if we don't have the game file data
        if not game_data or game_data['appid'] == -1:
            game_data = query_steam_api_by_title(title, steam_games)
        if game_data:
            table.add_row([
                game_data['title'],
                game_data['score'],
                game_data['number_of_reviews'],
                ', '.join(game_data['tags'])
            ])
            game_stats[title] = game_data
        else:
            table.add_row([title, 'N/A', 'N/A', 'N/A'])
            game_stats[title] = game_data

        # write the json table so I can easily query it later
        # write on each loop in case the program fails early
        # with open(output_file_name, 'w') as gs:
        #     json.dump(game_stats, gs, indent=4)


    # parse through the players steam games 
    # query the steam library and write to the file 
    
    # uncomment if only doing steam games
    # total_games = owned_steam_games['response']['game_count']


    for game in owned_steam_games['response']['games']:

        app_id = game['appid']

        update_progress(i, total_games, app_id)
        i += 1

        game_data = game_stats.get(app_id)
        # only query steam if we don't have the game file data
        if not game_data:
            game_data = query_steam_api(app_id, steam_games)
        if game_data:
            title = game_data['title']
            table.add_row([
                game_data['title'],
                game_data['score'],
                game_data['number_of_reviews'],
                ', '.join(game_data['tags'])
            ])
            game_stats[title] = game_data
        # else:
        #     table.add_row([title, 'N/A', 'N/A', 'N/A'])
        #     game_stats[title] = game_data
        # write the json table so I can easily query it later
        # write on each loop in case the program fails early
        # with open(output_file_name, 'w') as gs:
        #     json.dump(game_stats, gs, indent=4)

    # write the json table so I can easily query it later
    with open(output_file_name, 'w') as gs:
        json.dump(game_stats, gs, indent=4)

    print(table)

    print("*****************")
    print(f"not found games: {len(not_found_games)}")
    # for game in not_found_games:
    #     print(game)

    with open('notfound.txt', 'w') as nf:
        json.dump(not_found_games, nf, indent=4)
    print(f'\nFailed to Find {len(not_found_games)}')




if __name__ == '__main__':
    output_file_name = 'gamestats.json'
    main(output_file_name)
