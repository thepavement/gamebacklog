#! /bin/bash

# This script assumes heroic games launcher is installed and the user has logged into GOG and Epic Games

# usage getGamesList.sh | sort -u > gameslist.txt

# FLATPACK
GOG_LIBRARY_PATH="$HOME/.var/app/com.heroicgameslauncher.hgl/config/heroic/store_cache/gog_library.json"
LEGENDARY_LIBRARY_PATH="$HOME/.var/app/com.heroicgameslauncher.hgl/config/heroic/store_cache/legendary_library.json"
AMAZON_LIBRARY_PATH="$HOME/.var/app/com.heroicgameslauncher.hgl/config/heroic/store_cache/nile_library.json"


# APP IMAGE
# GOG_LIBRARY_PATH=~/.config/heroic/store_cache/gog_library.json
# LEGENDARY_LIBRARY_PATH=~/.config/heroic/store_cache/legendary_library.json

# this script just queries the gameslist from gog and epic games (legendary) and outputs the name
jq -r '.games[].title' ${GOG_LIBRARY_PATH}
jq -r '.library[].title' ${LEGENDARY_LIBRARY_PATH}
jq -r '.library[].title' ${AMAZON_LIBRARY_PATH}
