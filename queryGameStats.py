#! /usr/bin/python

import argparse
import json
from prettytable import PrettyTable


# sample usage: 
# ./queryGameStats.py --json gamestats.json --sort title --filter Indie --name "game name" --score "positive" --output table
# ./queryGameStats.py --json gamestats.json --sort title --filter Indie --name "game name" --score "positive" --output json


# Parse command line arguments
parser = argparse.ArgumentParser(description='Sort and filter game details from a JSON file.')
parser.add_argument('-s', '--sort', choices=['title', 'score', 'number_of_reviews'], help='Sort by this field.')
parser.add_argument('-f', '--filter', nargs='+', help='Filter games by these tags (space-separated).')
parser.add_argument('-n', '--name', help='Filter games by name.')
parser.add_argument('-c', '--score', help='Filter games by score.')
parser.add_argument('-j', '--json', required=True, help='Path to the JSON file containing game details.')
parser.add_argument('-o', '--output', choices=['json', 'table'], default='json', help='Output format: json or table.')
args = parser.parse_args()

# Load JSON data from the specified file
with open(args.json, 'r') as file:
    data = json.load(file)

# Filter function
def filter_games(games):
    filtered_games = {}
    for game_id, details in games.items():
        # Convert to lower case for case-insensitive comparison
        if args.name and args.name.lower() not in str(details['title']).lower():
            continue
        if args.score and args.score.lower() not in details['score'].lower():
            continue
        if args.filter and not all(tag.lower() in [t.lower() for t in details['tags']] for tag in args.filter):
            continue
        filtered_games[game_id] = details
    return filtered_games

# Apply filters
data = filter_games(data)

# Sort the games
if args.sort == 'number_of_reviews':
    sorted_data = sorted(data.items(), key=lambda x: int(x[1][args.sort]))
else:
    sorted_data = sorted(data.items(), key=lambda x: x[1].get(args.sort, '').lower())

# Choose output format
if args.output == 'json':
    print(json.dumps({game_id: details for game_id, details in sorted_data}, indent=4))
else:  # PrettyTable
    table = PrettyTable()
    table.field_names = ["Title", "Score", "Number of Reviews", "Tags"]
    table.max_width['Tags'] = 40
    for game_id, details in sorted_data:
        table.add_row([details['title'], details['score'], details['number_of_reviews'], ", ".join(details['tags'])])
    print(table)
